
document.querySelector('#push').onclick= function
(){
    // Ajouter validation pour un case input vide    
    if(document.querySelector('#newtask input').value.length == 0){
        alert("Please Enter a Task")
        
    }
    else{
        // SUr la base de cette div, on met le HTML  À LA INTERIOR  
        document.querySelector('#tasks').innerHTML 
        += `
        <div class="task">
        <span id="taskname">
        
        ${document.querySelector('#newtask input').value}   <!-- Pour recuperer le valeur (VALUE) y le mettre dans LE SPAN-->
        </span>
        <button class="delete">
        <i class="fas fa-trash-alt"></i>
        </button> 
        </div>
        
        `;
        let current_tasks = document.querySelectorAll(".delete");
        for(let i=0; i<current_tasks.length; i++){
            current_tasks[i].onclick= function(){
                this.parentNode.remove(); // Parent NOde pour la structure de HMTL 
            }
        }
        
        // Pour rayer une tâche realisé
        
        let tasks = document.querySelectorAll(".task");
        for(let i=0; i<tasks.length; i++){
            tasks[i].onclick = function(){
                this.classList.toggle('completed');
            }
        }
        
        // Pour effacer le champ de saisie après chaque entrée
        
        document.querySelector("#newtask input").value = "";
    }
    }